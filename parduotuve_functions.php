<?php
function isfailo ()
{
    $filename = "./prekes.json";
        if (is_readable($filename))
        {
            $filecontents = file_get_contents("./prekes.json");
            $json_data = json_decode($filecontents,true);
            $prekiumasyvas = array();

            if (!empty($json_data))
            {
                foreach ($json_data as $i => $e):
                    array_push($prekiumasyvas, ["id" => $i, "preke" => $e["preke"], "kaina" => $e["kaina"]]);
                endforeach; 
            }
        }
        else
        {
            echo "Prekių failo nebuvo įmanoma nuskaityti.";
        }       
        return $prekiumasyvas;
}

function gauti_duombaze() {
    try {
        $pdo = new PDO('mysql:host=localhost', 'root', ''); #localhost arba 127.0.0.1
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    } catch (PDOException $e) {
        echo $e;
        return 'Error';
    }
}

function isduombazes() {
    $pdo = gauti_duombaze();
    $pdo->prepare('USE parduotuve')->execute();
    $prekes = $pdo->prepare('SELECT * FROM prekes;');
    $prekes->execute();
    return $prekes->fetchAll(PDO::FETCH_CLASS);
}

function irduombaze($preke, $kaina) {
    $pdo = gauti_duombaze();
    $pdo->prepare('USE parduotuve')->execute();
    $kaina = $kaina;
    $uzklausa = $pdo->prepare('INSERT INTO prekes (preke, kaina) VALUES (:preke, :kaina);');
    $uzklausa->bindParam(':preke', $preke);
    $uzklausa->bindParam(':kaina', $kaina);
    $uzklausa->execute();
}

function isduombaze($id) {
    $pdo = gauti_duombaze();
    $pdo->prepare('USE parduotuve')->execute();
    $uzklausa = $pdo->prepare('DELETE FROM prekes WHERE ID=:id');
    $uzklausa->bindParam(':id', $id);
    if ($uzklausa->execute()) {
        return true;
    } else {
        return false;
    }
}
 function reduombaze($id, $preke, $kaina) {
    $kaina = $kaina;
    $pdo = gauti_duombaze();
    $pdo->prepare('USE parduotuve')->execute();
    $uzklausa = $pdo->prepare('UPDATE prekes SET preke=:preke, kaina=:kaina WHERE ID=:id');
    $uzklausa->bindParam(':preke', $preke);
    $uzklausa->bindParam(':kaina', $kaina);
    $uzklausa->bindParam(':id', $id);
    if ($uzklausa->execute()) {
        return true;
    }
    return false;
 }